Source: rocdbgapi
Section: devel
Homepage: https://github.com/ROCm/ROCdbgapi
Priority: optional
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rocm-team/rocdbgapi.git
Vcs-Browser: https://salsa.debian.org/rocm-team/rocdbgapi
Maintainer: Debian ROCm Team <debian-ai@lists.debian.org>
Uploaders: Cordell Bloor <cgmb@slerp.xyz>,
Build-Depends: debhelper-compat (= 13),
               cmake,
               libamd-comgr-dev (>= 5.7.1~),
               libhsa-runtime-dev,
               pci.ids
Rules-Requires-Root: no

Package: librocm-dbgapi0
Section: libs
Architecture: amd64 arm64 ppc64el
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: AMD GPU debugger support - library
 The ROCdbpgapi library provides debugging routines to inspect and
 control programs running on AMD GPUs. This library is used by
 debuggers to receive notifications of events, query the GPU state,
 and step through execution. However, operations such as symbolic
 mappings, code object decoding and stack unwinding are outside the
 scope of this library.
 .
 This package provides the ROCdbgapi library.

Package: amd-dbgapi-dev
Section: libdevel
Architecture: amd64 arm64 ppc64el
Depends: librocm-dbgapi0 (= ${binary:Version}),${misc:Depends}, ${shlibs:Depends},
Description: AMD GPU debugger support - headers
 The ROCdbpgapi library provides debugging routines to inspect and
 control programs running on AMD GPUs. This library is used by
 debuggers to receive notifications of events, query the GPU state,
 and step through execution. However, operations such as symbolic
 mappings, code object decoding and stack unwinding are outside the
 scope of this library.
 .
 This package provides the ROCdbgapi development headers.
